#!/usr/bin/python3

import numpy
import random as rd

#N number of nodes; k mean conections for nodes; mu interconectivity
def randomGraph(N, k):
    #sanity check
    if k >= N:
        print("You have chosen an invalid value of k. It must be smaller than N!")

    #stores the neigbours of each node
    neigs = []
    for i in range(N):
        neigs.append([])

    #each interaction of the loop adds one edge to the network
    #k has to be divided by 2 because a single edge counts for 2 nodes
    for j in range(int(N*k/2.)):
        print("%i-th vertex."%(j))
        #this garantees that we will have selected two nodes
        while True:
            node1 = rd.randint(0,N-1)
            availiableNodes = list(range(N))

            availiableNodes.remove(node1)
            for element in neigs[node1]:
                availiableNodes.remove(element)

            if len(availiableNodes) > 0:
                break

        node2 = rd.choice(availiableNodes)
        #print(node1)
        neigs[node1].append(node2)
        neigs[node2].append(node1)

    with open("neigVec.txt","w") as output:
        output.write("%i\n"%(N))
        for neigbour in neigs:
            output.write("%i"%(len(neigbour)))
            for node2 in neigbour:
                output.write(" %i"%(node2))
            output.write("\n")

def randomModularGraph(N, n1, k, mu):
    #sanity check
    if k >= N:
        print("You have chosen an invalid value of k. It must be smaller than N!")

    #stores the neigbours of each node
    neigs = []
    for i in range(N):
        neigs.append([])

    half = int(N*n1)

    #each interaction of the loop adds one edge to the network
    #k has to be divided by 2 because a single edge counts for 2 nodes
    for j in range(int((1.-mu)*N*k/2.)):
        #this garantees that we will have selected a node that can be connected to another node in the same community
        while True:
            node1 = rd.randint(0,N-1)
            if node1 < half:
                availiableNodes = list(range(half))
            else:
                availiableNodes = list(range(half,N))

            availiableNodes.remove(node1)
            for element in neigs[node1]:
                if element in availiableNodes:
                    availiableNodes.remove(element)

            if len(availiableNodes) > 0:
                break

        node2 = rd.choice(availiableNodes)
        #print(node1)
        neigs[node1].append(node2)
        neigs[node2].append(node1)

    for j in range(int(mu*N*k/2.)):
        #this garantees that we will have selected a node that can be connected to another node in the differet communities
        availiableNodes = []
        while availiableNodes == []:
            node1 = rd.randint(0,N-1)
            if node1 >= half:
                availiableNodes = list(range(half))
            else:
                availiableNodes = list(range(half,N))
            for element in neigs[node1]:
                if element in availiableNodes:
                    availiableNodes.remove(element)

        node2 = rd.choice(availiableNodes)
        neigs[node1].append(node2)
        neigs[node2].append(node1)

    with open("MD_N_%05d_k_%04d_mu_%f.txt"%(N,k,mu),"w") as output:
        output.write("%i\n"%(N))
        for neigbour in neigs:
            output.write("%i"%(len(neigbour)))
            for node2 in neigbour:
                output.write(" %i"%(node2))
            output.write("\n")

#hs = [0.25]#numpy.arange(0.41,0.5,0.01)

#def randomModularGraph(N, n1, k, mu):
randomModularGraph(10000, 0.5, 30., 0.2)
