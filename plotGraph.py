#!/usr/bin/python3

from igraph import *
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np
import random as rd


plt.rc('text', usetex=True)
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 16}
plt.rc('font', **font)
fig, ax = plt.subplots()

def fromFile (filename):
    G = Graph()
    with open(filename, "r") as nVec:
        lines = nVec.readlines()
        N = int(lines[0])
        G.add_vertices(N)
        for node1 in range(N):
            for neig in lines[node1+1].split("\t")[1:-1]:
                if G.get_eid(node1, int(neig), directed=False, error=False) == -1:
                    G.add_edge(node1,int(neig))
    return G

def randomModularGraphPlot(N, n1, k, mu):
    #sanity check
    if k >= N:
        print("You have chosen an invalid value of k. It must be smaller than N!")

    #stores the neigbours of each node
    G = Graph()
    G.add_vertices(N)

    half = int(N*n1)

    #each interaction of the loop adds one edge to the network
    #k has to be divided by 2 because a single edge counts for 2 nodes
    for j in range(int((1.-mu)*N*k/2.)):
        #this garantees that we will have selected a node that can be connected to another node in the same community
        availiableNodes = []
        while availiableNodes == []:
            node1 = rd.randint(0,N-1)
            if node1 < half:
                availiableNodes = list(range(half))
            else:
                availiableNodes = list(range(half,N))

            availiableNodes.remove(node1)
            for element in G.neighbors(node1):
                if element in availiableNodes:
                    availiableNodes.remove(element)

        node2 = rd.choice(availiableNodes)
        #print(node1)
        G.add_edge(node1,node2)

    for j in range(int(mu*N*k/2.)):
        #this garantees that we will have selected a node that can be connected to another node in the differet communities
        availiableNodes = []
        while availiableNodes == []:
            node1 = rd.randint(0,N-1)
            if node1 >= half:
                availiableNodes = list(range(half))
            else:
                availiableNodes = list(range(half,N))
            for element in G.neighbors(node1):
                if element in availiableNodes:
                    availiableNodes.remove(element)

        node2 = rd.choice(availiableNodes)
        G.add_edge(node1,node2)

    colors = []
    shapes = []
    for i in range(half):
        colors.append("green")
        shapes.append("triangle")
    for i in range(half,N):
        colors.append("orange")
        shapes.append("diamond")
    G.vs['color'] = colors
    G.vs['shape'] = shapes
    plot(G,"ER_mod_%lf.eps"%(mu), background="white", mark_groups=True)


def plotBA(N,m):
    graph = Graph.Barabasi(N,m)
    colors = []
    shapes = []
    for i in range(N):
        colors.append("red")
        shapes.append("circle")
    graph.vs['color'] = colors
    graph.vs['shape'] = shapes
    plot(graph,"BA.eps", background="white", mark_groups=True)

    #fig, ax = plt.subplots()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.xlabel("$k$")
    plt.ylabel("$p(k)$")
    ax.set_xlim([1.8,28])
    degree = graph.degree()
    major = max(degree)
    minor = min(degree)
    bins = np.arange(minor-1, major+1)
    plt.hist(degree, bins=bins, density=True)

    inset = inset_axes(ax, width="60%", height="60%", loc=1)
    inset.set_xscale('log')
    inset.set_xlabel('$k$')
    inset.set_xlim([1.8,30])
    inset.set_ylabel('$p(k)$')
    inset.set_yscale('log')
    inset.hist(degree, bins=bins, density=True)

    #plt.tight_layout()
    plt.savefig("BA_degreeDist.png")
    plt.show()

def plotWS(N,k,p):
    nei = int(k/2)
    graph = Graph.Watts_Strogatz(1,N,nei,p)
    colors = []
    shapes = []
    for i in range(N):
        colors.append("red")
        shapes.append("circle")
    graph.vs['color'] = colors
    graph.vs['shape'] = shapes
    plt.tight_layout()
    plot(graph,"WS_p_%02d.eps"%(int(100*p)), background="white", mark_groups=True, layout="circle")
    #fig.savefig("WS_degreeDist.eps")

    #plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    #fig, ax = plt.subplots()
    #ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    #plt.xlabel("$k$")
    #plt.ylabel("$p(k)$")
    #degree = graph.degree()
    #major = max(degree)
    #minor = min(degree)
    #bins = np.arange(minor-0.5, major+0.5)
    #plt.hist(degree, bins=bins, density=True)
    #fig.savefig("WS_degreeDist.eps")

def plotKC():
    graph = Graph()
    graph.add_vertices(34)
    links = [(1,2),(1,3),(2,3),(1,4),(2,4),(3,4),(1,5),(1,6),(1,7),(5,7),(6,7),(1,8),(2,8),(3,8),(4,8),(1,9),(3,9),(3,10),(1,11),(5,11),(6,11),(1,12),(1,13),(4,13),(1,14),(2,14),(3,14),(4,14),(6,17),(7,17),(1,18),(2,18),(1,20),(2,20),(1,22),(2,22),(24,26),(25,26),(3,28),(24,28),(25,28),(3,29),(24,30),(27,30),(2,31),(9,31),(1,32),(25,32),(26,32),(29,32),(3,33),(9,33),(15,33),(16,33),(19,33),(21,33),(23,33),(24,33),(30,33),(31,33),(32,33),(9,34),(10,34),(14,34),(15,34),(16,34),(19,34),(20,34),(21,34),(23,34),(24,34),(27,34),(28,34),(29,34),(30,34),(31,34),(32,34),(33,34)]
    for edge in links:
        a,b = edge
        graph.add_edge(a-1,b-1)
    colors = []
    shapes = []
    for i in range(34):
        colors.append("red")
        shapes.append("circle")
    graph.vs['color'] = colors
    graph.vs['shape'] = shapes
    plot(graph,"KC.eps", background="white", mark_groups=True)

def plotER(N,k):
    M = int(N*k/2)
    graph = Graph.Erdos_Renyi(N,m=M)
    colors = []
    shapes = []
    for i in range(N):
        colors.append("red")
        shapes.append("circle")
    graph.vs['color'] = colors
    graph.vs['shape'] = shapes
    plot(graph,"ER.eps", background="white", mark_groups=True)

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    degree = graph.degree()
    major = max(degree)
    minor = min(degree)
    bins = np.arange(minor-0.5, major+0.5)
    plt.hist(degree, bins=bins, density=True)
    plt.xlabel("$k$")
    plt.ylabel("$p(k)$")
    plt.tight_layout()
    fig.savefig("ER_degreeDist.eps")


def plotFile():
    graph = fromFile("Nvec.txt")

    colors = []
    shapes = []
    for i in range(50):
        colors.append("red")
        shapes.append("circle")
    for i in range(50,100):
        colors.append("blue")
        shapes.append("square")
    graph.vs['color'] = colors
    graph.vs['shape'] = shapes
    plot(graph,"BA_mod.pdf", background="white", mark_groups=True)

for mus in [0.05,0.1,0.3]:
    randomModularGraphPlot(100,0.5,10,mus)

#ps = [0,0.1,0.6,1.0]
#for p in ps:
#    plotWS(20,2,p)
