#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX(a,b) (((a)>(b))?(a):(b))

void shuffle(int *array, size_t n)
{
    if (n > 1)
    {
        size_t i;
        for (i = 0; i < n - 1; i++)
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}

double grand()
{
    return (double) rand()/RAND_MAX;
}

int compare (const void * a, const void * b)
{
      return ( *(int*)a - *(int*)b );
}

typedef struct Lista list;
struct Lista {
    int allocated;
    int leng;
    int *list;
};

list initList(int size)
{
    list lista;
    lista.allocated = size;
    lista.leng = 0;
    lista.list = (int*) malloc(lista.allocated* sizeof(int));
    return lista;
}

void resizeList(list *lista, int size)
{
    lista->allocated += size;
    lista->list = (int*) realloc(lista->list, lista->allocated* sizeof(int));
}

//this implements a binary search to find the index to which the new value should be appended
int findApIndex(list *lista, int *value)
{
    int low,high,mid;
    low = 0;
    high = lista->leng-1;
    mid = (high + low)/2;
    while (high > low)
    {
        if (compare(&lista->list[mid], value) > 0)
            high = mid;
        else
            low = mid+1;
        mid = (high + low)/2;
    }
    return mid;
}

int finddpIndex(double *lista, int *size, double *value)
{
    int low,high,mid;
    low = 0;
    high = *size -1;
    mid = (high + low)/2;
    while (high > low)
    {
        if (lista[mid] > *value)
            high = mid;
        else
            low = mid+1;
        mid = (high + low)/2;
    }
    return mid;
}

void appendList(list *lista, int value)
{
    //checks if the allocated space is enough
    if (lista->leng+1 > lista->allocated) resizeList(lista, 10);

    //finds where the new value should be appended
    if (lista->leng > 0 && lista->list[lista->leng-1] > value)
    {
        int index = findApIndex(lista, &value);
        //shifts all the bigger values
        printf("index = %i old %i new %i\n",index,lista->list[index],value);
        for (int i=lista->leng;i>index;i--) {
            lista->list[i] = lista->list[i-1];
        }
        lista->list[index] = value;
    }
    else
        lista->list[lista->leng] = value;

    lista->leng++;
}

int randomGraph(int N, float k)
{
    int adjM[N][N];
    int auxN[N];
    int i,j,node1,node2,count,nCount;
    for (i=0;i<N;i++)
    {
        for (j=0;j<N;j++)
        {
            adjM[i][j] = 0;
        }
    }
    nCount = 0;
    while (nCount<(int) N*k/2.)
    {
        node1 = rand()%N;
        for (i=0;i<N;i++) auxN[i] = 0;
        count = 0;
        for (i=0;i<N;i++)
        {
            if (! adjM[node1][i])
            {
                auxN[count] = i;
                count++;
            }
        }
        if (count > 0)
        {
            node2 = auxN[rand()%count];
            adjM[node1][node2] = 1;
            adjM[node2][node1] = 1;
            nCount++;
        }
    }
    printf("%i\n", N);
    for (i=0;i<N;i++)
    {
        for (j=0;j<N;j++) auxN[j] = 0;
        count = 0;
        for (j=0;j<N;j++)
        {
            if (adjM[i][j])
            {
                auxN[count] = j;
                count++;
            }
        }
        printf("%i\t", count);
        for (j=0;j<count;j++) printf("%i\t", auxN[j]);
        printf("\n");
    }
    return 0;
}

int isInList(list *lista, int value)
{
    char *pItem;
    pItem = (char*) bsearch (&value, lista->list, lista->leng, sizeof(int), compare);
    if (pItem == NULL) return 0;
    else return 1;
}

void printfGraph(list *lista, int N)
{
    printf("%i\n", N);
    for (int i=0;i<N;i++)
    {
        printf("%i\t", lista[i].leng);
        for (int j=0;j<lista[i].leng;j++) printf("%i\t", lista[i].list[j]);
        printf("\n");
    }
}

void clearMem(list* lista, int N)
{
    for(int i=0;i<N;i++)
    {
        free(lista[i].list);
    }

    free(lista);
}

list* randomModularGraph(int N, int n1, float k, float mu)
{
    int i,j,node1,node2,count,nCount;
    list* Nvec;
    Nvec = (list*) malloc(N*sizeof(list));
    for (i=0;i<N;i++) Nvec[i] = initList(k);
    int auxN[N];

    nCount = 0;
    while (nCount<(int) N*(1.-mu)*k/2.)
    {
        node1 = rand()%N;
        //printf("node1 = %i\n", node1);
        if (node1 < n1)
        {
            //reinitializes the options
            for (i=0;i<n1;i++) auxN[i] = 0;
            count = 0;
            for (i=0;i<n1;i++)
            {
                //check if connection is availiable
                if (i != node1 && (! isInList(&Nvec[node1], i)))
                {
                    auxN[count] = i;
                    count++;
                }
            }
            //printf("Avail: ");
            //for (i=0;i<count;i++) printf("%i  ",auxN[i]);
            //printf("\n");
            if (count > 0)
            {
                node2 = auxN[rand()%count];
                appendList(&Nvec[node1],node2);
                appendList(&Nvec[node2],node1);
                nCount++;
            }
        }
        else
        {
            //reinitializes the options
            for (i=n1;i<N;i++) auxN[i] = 0;
            count = 0;
            for (i=n1;i<N;i++)
            {
                //check if connection is availiable
                if (i != node1 && ! isInList(&Nvec[node1], i))
                {
                    auxN[count] = i;
                    count++;
                }
            }
            //printf("Avail: ");
            //for (i=0;i<count;i++) printf("%i  ",auxN[i]);
            //printf("\n");
            if (count > 0)
            {
                node2 = auxN[rand()%count];
                appendList(&Nvec[node1],node2);
                appendList(&Nvec[node2],node1);
                nCount++;
            }
        }
    }
    nCount = 0;
    while (nCount<(int) N*mu*k/2.)
    {
        node1 = rand()%N;
        if (node1 > n1)
        {
            //reinitializes the options
            for (i=0;i<n1;i++) auxN[i] = 0;
            count = 0;
            for (i=0;i<n1;i++)
            {
                if (! isInList(&Nvec[node1], i))
                {
                    auxN[count] = i;
                    count++;
                }
            }
            if (count > 0)
            {
                node2 = auxN[rand()%count];
                appendList(&Nvec[node1],node2);
                appendList(&Nvec[node2],node1);
                nCount++;
            }
        }
        else
        {
            //reinitializes the options
            for (i=n1;i<N;i++) auxN[i] = 0;
            count = 0;
            for (i=n1;i<N;i++)
            {
                if (! isInList(&Nvec[node1], i))
                {
                    auxN[count] = i;
                    count++;
                }
            }
            if (count > 0)
            {
                node2 = auxN[rand()%count];
                appendList(&Nvec[node1],node2);
                appendList(&Nvec[node2],node1);
                nCount++;
            }
        }
    }
    return Nvec;
}

list* modularBA(int N, int m, double n1, double h)
{
    int ndComm = (int) N*n1;
    int i,j,addedNodes;
    list* Nvec;
    Nvec = (list*) malloc(N*sizeof(list));
    int addOrder[N];
    for (i=0;i<N;i++)
    {
        Nvec[i] = initList(m+1);
        addOrder[i] = i;
    }
    shuffle(addOrder, N);
    //the node m+1 is added and connected to all the m nodes that came before it
    for (i=0;i<m;i++)
    {
        //printf("%i\t",addOrder[i]);
        appendList(&Nvec[addOrder[m]],addOrder[i]);
        appendList(&Nvec[addOrder[i]],addOrder[m]);
    }
    //printf("%i  \n#########\n",addOrder[m]);
    double probs[N], sumProbs[N], rdVal,totProb;
    int node2,k,notConn[N],countNC;
    for (i=(m+1);i<N;i++)
    {
        //printf("%i\t",addOrder[i]);
        for (j=0;j<i;j++)
        {
            //the exclusive or garantees that it is only true when they bellong to different communities
            if ((addOrder[i] < ndComm) ^ (addOrder[j] < ndComm))
                probs[j] = (double) Nvec[addOrder[j]].leng*(h);
            else
                probs[j] = (double) Nvec[addOrder[j]].leng*(1.-h);
        }
        for (k=0;k<m;k++)
        {
            totProb = 0.0;
            countNC = 0;
            for (j=0;j<i;j++)
            {
                if (! isInList(&Nvec[addOrder[i]], addOrder[j]))
                {
                    notConn[countNC] = addOrder[j];
                    totProb += probs[j];
                    sumProbs[countNC] = totProb;
                    countNC++;
                }
            }
            rdVal = grand()*totProb;
            node2 = finddpIndex(sumProbs,&countNC,&rdVal);
            //printf("%g < %g < %g\n",sumProbs[MAX(0,node2-1)],rdVal,sumProbs[node2]);
            appendList(&Nvec[notConn[node2]],addOrder[i]);
            appendList(&Nvec[addOrder[i]],notConn[node2]);
            //printf("%i\t",notConn[node2]);
        }
    }
    return Nvec;
}

int main ()
{
    srand(time(NULL));

    int N = 100;
    list* meme = modularBA(N,1,0.5,0.0);
    printfGraph(meme,N);
    clearMem(meme,N);

    return 0;
}
